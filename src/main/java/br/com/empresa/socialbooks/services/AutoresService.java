package br.com.empresa.socialbooks.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.empresa.socialbooks.domain.Autor;
import br.com.empresa.socialbooks.repository.AutoresRepository;
import br.com.empresa.socialbooks.services.exceptions.AutorExistenteException;
import br.com.empresa.socialbooks.services.exceptions.AutorNaoEncontradoException;

@Service
public class AutoresService {

	@Autowired
	private AutoresRepository autoresRepository;

	public List<Autor> listar() {
		return autoresRepository.findAll();
	}

	public Autor buscar(Long id) {
		Autor autor = autoresRepository.findOne(id);

		if (autor == null) {
			throw new AutorNaoEncontradoException("Autor não encontrado");
		}

		return autor;
	}

	public Autor salvar(Autor autor) {
		if (autor.getId() != null) {
			Autor a = autoresRepository.findOne(autor.getId());

			if (a != null) {
				throw new AutorExistenteException("Autor já existente");
			}
		}

		return autoresRepository.save(autor);
	}

}

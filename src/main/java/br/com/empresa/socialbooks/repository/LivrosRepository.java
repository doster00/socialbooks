package br.com.empresa.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.socialbooks.domain.Livro;

public interface LivrosRepository extends JpaRepository<Livro, Long> {

}

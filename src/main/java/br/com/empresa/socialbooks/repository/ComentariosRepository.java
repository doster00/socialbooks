package br.com.empresa.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.socialbooks.domain.Comentario;

public interface ComentariosRepository extends JpaRepository<Comentario, Long> {

}

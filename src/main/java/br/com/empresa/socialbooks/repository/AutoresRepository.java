package br.com.empresa.socialbooks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.empresa.socialbooks.domain.Autor;

public interface AutoresRepository extends JpaRepository<Autor, Long> {

}

package br.com.empresa.socialbooks.handler;

import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.empresa.socialbooks.domain.DetalhesErro;
import br.com.empresa.socialbooks.services.exceptions.AutorExistenteException;
import br.com.empresa.socialbooks.services.exceptions.AutorNaoEncontradoException;
import br.com.empresa.socialbooks.services.exceptions.LivroNaoEncontradoException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(LivroNaoEncontradoException.class)
	public ResponseEntity<DetalhesErro> handleLivroNaoEncontradoException(LivroNaoEncontradoException e, HttpServletRequest request) {
		DetalhesErro detalhesErro = new DetalhesErro();

		detalhesErro.setStatus(404L);
		detalhesErro.setTitulo("O livro não foi encontrado");
		detalhesErro.setMensagemDesenvolvedor("http://errors.socialbooks.com/404");
		detalhesErro.setTimestamp(new Timestamp(new Date().getTime()));

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(detalhesErro);
	}

	@ExceptionHandler(AutorNaoEncontradoException.class)
	public ResponseEntity<DetalhesErro> handleAutorNaoEncontradoException(AutorNaoEncontradoException e, HttpServletRequest request) {
		DetalhesErro detalhesErro = new DetalhesErro();

		detalhesErro.setStatus(404L);
		detalhesErro.setTitulo("Autor não encontrado");
		detalhesErro.setMensagemDesenvolvedor("http://errors.socialbooks.com/404");
		detalhesErro.setTimestamp(new Timestamp(new Date().getTime()));

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(detalhesErro);
	}

	@ExceptionHandler(AutorExistenteException.class)
	public ResponseEntity<DetalhesErro> handleAutorExistenteException(AutorExistenteException e, HttpServletRequest request) {
		DetalhesErro detalhesErro = new DetalhesErro();

		detalhesErro.setStatus(409L);
		detalhesErro.setTitulo("O autor já existente");
		detalhesErro.setMensagemDesenvolvedor("http://errors.socialbooks.com/409");
		detalhesErro.setTimestamp(new Timestamp(new Date().getTime()));

		return ResponseEntity.status(HttpStatus.CONFLICT).body(detalhesErro);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity<DetalhesErro> handleDataIntegrityViolationException(DataIntegrityViolationException e, HttpServletRequest request) {
		DetalhesErro detalhesErro = new DetalhesErro();

		detalhesErro.setStatus(400L);
		detalhesErro.setTitulo("Requisição inválida");
		detalhesErro.setMensagemDesenvolvedor("http://errors.socialbooks.com/400");
		detalhesErro.setTimestamp(new Timestamp(new Date().getTime()));

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(detalhesErro);
	}

}
